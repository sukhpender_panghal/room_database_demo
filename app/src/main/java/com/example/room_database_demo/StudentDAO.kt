package com.example.room_database_demo

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface StudentDAO {

    @Insert()
    fun insert(studentClass: StudentClass)
    @Query("select * from student ORDER BY name ASC")
    fun getStudentList():List<StudentClass>

}