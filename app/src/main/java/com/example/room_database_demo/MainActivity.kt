package com.example.room_database_demo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var db : StudentDataBase? = null
    private var studentDao : StudentDAO? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerview.layoutManager = LinearLayoutManager(this)
        btn_click.setOnClickListener {

            Observable.fromCallable {

                db = StudentDataBase.getAppDataBase(this)
                studentDao = db?.StudentDAO()

                val student = StudentClass(11,"naveen",32)
                with(studentDao){
                    this?.insert(student)
                }
                db?.StudentDAO()?.getStudentList()
            }.subscribeOn(Schedulers.io()).subscribe {
                Log.d("size",""+it?.size.toString())
                recyclerview.adapter = StudentAdapter(it!!,this)
            }
        }
    }
}