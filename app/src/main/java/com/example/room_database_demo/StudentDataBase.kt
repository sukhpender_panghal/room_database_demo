package com.example.room_database_demo

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(StudentClass::class),version = 1)
abstract class StudentDataBase : RoomDatabase()
{
    abstract fun StudentDAO():StudentDAO

    companion object{

        var INSTANCE : StudentDataBase? = null
        fun getAppDataBase(context : Context):StudentDataBase?
        {
            if (INSTANCE == null){
                synchronized(StudentDataBase::class)
                {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        StudentDataBase::class.java,
                        "studentdb").build()
                }
            }

            return INSTANCE
        }
    }
}