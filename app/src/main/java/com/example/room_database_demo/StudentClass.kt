package com.example.room_database_demo

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "student")
class StudentClass(

    @PrimaryKey(autoGenerate = true) var id : Int,
    @ColumnInfo(name = "name") var studentname:String,
    @ColumnInfo(name = "age") var age : Int

)
